import java.util.ArrayList;
import java.util.List;

public class CacheUpdate implements CacheOperation, Runnable {
	
	private long refresh = 600000; // 10 minut
	private Cache cache = Cache.getInstance();
	private String key;
	private List<EnumValue> items = new ArrayList<EnumValue>();

	public void items(String key, List<EnumValue> items) {
		this.items = items;;
		this.key = key;
	}
	
	@Override
	public synchronized void run() {	
		while(true) {
			operation();
			try {
				Thread.sleep(refresh);
			} catch (InterruptedException e) {
			e.printStackTrace();
			}
		}
	}

	public void operation() {
		cache.put(key, items);
		System.gc();
	}

}