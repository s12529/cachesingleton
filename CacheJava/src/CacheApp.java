import java.util.ArrayList;
import java.util.List;

public class CacheApp {

	public static void main(String[] args) {
			
		CacheUpdate update = new CacheUpdate();	
		Thread t1 = new Thread(update);
		Thread t2 = new Thread(update);

		List<EnumValue> persons = new ArrayList<EnumValue>();
		List<EnumValue> regions = new ArrayList<EnumValue>();
		
		persons.add(new EnumValue(1, 2, "KOD", "Robert", "OSOBA"));
		persons.add(new EnumValue(2, 3, "KOA", "Hubert", "OSOBA"));
		persons.add(new EnumValue(3, 4, "KON", "Pina", "OSOBA"));	
		
		regions.add(new EnumValue(1, 4, "REG", "Pomorskie", "REGION"));
		regions.add(new EnumValue(2, 6, "REG", "Mazowieckie", "REGION"));
		regions.add(new EnumValue(3, 3, "REG", "Lubelskie", "REGION"));
		
		update.items("OSOBA", persons);
		t1.run();
		update.items("REGION", regions);
		t2.run();	
		
	}
	
}
